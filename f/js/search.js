﻿function liFormat (row, i, num) {
  var result = row[0]+'<br /><span style="font-size:11px;color:#666666;">'+ row[1] +'</span>';
  return result;
}
function selectItem(li) {
	var ac = $("#street")[0].autocompleter;
	var ac2 = $("#build")[0].autocompleter;
	if( !!li.extra ) var sValue = li.extra[1];
	else var sValue = li.selectValue;
	document.getElementById('mapid').value=sValue;
	ac.setExtraParams({city:document.getElementById('city').value, type:2});
	ac2.setExtraParams({city:document.getElementById('city').value, street:document.getElementById('street').value,type:3});
	$("#map").html('<img src="/images/icons/loader.gif" alt="Loading..." />');
	$("#map").load('/includes/browse/vmap.php?mapid='+sValue,{sValue:sValue});  
}


$(document).ready(function(){	
  $("#city").autocomplete("/includes/browse/vmap.php", {
    delay:10,
    minChars:3,
    matchSubset:1,
    autoFill:false,
    matchContains:1,
    cacheLength:1,
    selectFirst:true,
    formatItem:liFormat,
    maxItemsToShow:15,
    onItemSelect:selectItem,
	extraParams : {type:1}
  });

/* street */
  $("#street").autocomplete("/includes/browse/vmap.php", {
    delay:10,
    minChars:2,
    matchSubset:1,
    autoFill:false,
    matchContains:1,
    cacheLength:1,
    selectFirst:true,
    formatItem:liFormat,
    maxItemsToShow:15,
    onItemSelect:selectItem,
	extraParams : {type:2,city:document.getElementById('city').value}
  });

/* build */

  $("#build").autocomplete("/includes/browse/vmap.php", {
    delay:10,
    minChars:1,
    matchSubset:1,
    autoFill:false,
    matchContains:1,
    cacheLength:1,
    selectFirst:true,
    formatItem:liFormat,
    maxItemsToShow:15,
    onItemSelect:selectItem,
	extraParams : {city:document.getElementById('city').value, street:document.getElementById('street').value,type:3}
  });
});
